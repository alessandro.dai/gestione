var myHeaders = new Headers();
myHeaders.append("Authorization", "token 7601f574-da44-49a9-8e2a-8a8397553fde");

var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
};
var calendarG;
var eventId;
var activityID;

var urlEvent = "https://events.abattaglia.it/api/event";
var urlEventWithId = urlEvent + "/" + eventId;
var urlActivity = urlEventWithId + "/activity";
var urlActivityWithId = urlActivity + "/" + activityID;

window.mobilecheck = function () {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');
    calendarG = new FullCalendar.Calendar(calendarEl, {
        expandRows: true,
        height: '95%',

        handleWindowResize: true,

        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: (window.mobilecheck() && window.matchMedia("(orientation: portrait)").matches) ? 'timeGridWeek,timeGridDay,listWeek' : 'multiMonthYear,dayGridMonth,timeGridWeek,timeGridDay,listWeek',
        },

        initialView: (window.mobilecheck() && window.matchMedia("(orientation: portrait)").matches) ? 'timeGridDay' : 'dayGridMonth',
        navLinks: true,
        editable: false,
        selectable: true,
        nowIndicator: true,
        dayMaxEvents: true,
        weekNumbers: true,
        firstDay: '1',

        eventClick: function (info) {
            clearSomeThing("eventForm")
            appearForm();
            addActivityForm(info.event.id);
            getActivityList();
        },
        dateClick: function (info) {
            clickOrSelectEvent(info.dateStr, info.dateStr);
        },
        select: function (info) {
            clickOrSelectEvent(info.startStr, info.endStr)
        }
    });

    calendarG.render();
});

window.addEventListener('resize', () => {
    location.reload();
});

function initialization() {
    getEventList();

    let closeEventButton = document.getElementById("closeEvent");
    closeEventButton.addEventListener("click", disappearForm);
}

function clickOrSelectEvent(startStr, endStr) {
    clearSomeThing("eventForm");
    appearForm();
    addEventForm(startStr, endStr);
}

function getEventList() {

    let url = urlEvent + "/list";
    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            result.forEach(element => {
                let title = createElementP(element.title);
                let dateS = createElementP(element.startsAt);
                let dateE = createElementP(element.endsAt);
                let id = createElementP(element.id)

                addEventOnCalendar(title, dateS, dateE, id);
            });
        })
        .catch(error => showError(error));
}

function getActivityList() {

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let url = urlEventWithId + "/activity/list";

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            let container = document.getElementById("viewAllActivity");
            container.className = "col mb-5 carousel-inner";

            if (result.length === 0) {
                container.classList.add("d-none");

            } else {
                removeClass("panel", "d-none");
            }

            let i = 0;
            result.forEach(element => {
                containerDivAppendChild(container, element, "auction", i);
                i++;
            });

        })
        .catch(error => showError(error));
}

function getGuestList() {
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let url = urlActivityWithId + "/guest/list";

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            let i = 0;
            let container = document.getElementById("showGuestRow");
            checkIsEmpty(result, "showGuest");

            result.forEach(element => {
                containerDivAppendChild(container, element, "guest", i);
                i++;
            })

        })
        .catch(error => showError(error));
}

function getExhibitorList() {
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let url = urlActivityWithId + "/exhibitor/list";

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            let i = 0;
            let container = document.getElementById("showExhibitorRow");

            checkIsEmpty(result, "showExhibitor");

            result.forEach(element => {
                containerDivAppendChild(container, element, "exhibitor", i);
                i++;
            })
        })
        .catch(error => showError(error));
}

function getProductList() {
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let url = urlActivityWithId + "/product/list";

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            let i = 0;
            let container = document.getElementById("showProductRow");

            checkIsEmpty(result, "showProduct");

            result.forEach(element => {
                getProductDetail(element.id, container, i);
                i++;
            });
        })
        .catch(error => showError(error));
}

function getProductDetail(id, container, i) {
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let url = urlActivityWithId + "/product/" + id;
    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            containerDivAppendChild(container, result, "product", i);
        })
        .catch(error => showError(error));
}

function getElement(id) {
    return document.getElementById(id);
}

function addEventOnCalendar(title, dateS, dateE, id) {
    calendarG.addEvent({
        id: id.innerText,
        title: title.innerText,
        start: new Date(dateS.innerText),
        end: new Date(dateE.innerText),
        allDay: true
    });
}

function appearForm() {

    removeClass("bg-eventForm", "d-none");
    addClass("bg-eventForm", "d-inline");
    removeClass("activity", "d-none");
}

function disappearForm() {
    removeClass("bg-eventForm", "d-inline");
    addClass("bg-eventForm", "d-none");
    clearSomeThing("viewAllActivity");
    clearSomeThing("showProductRow");
    clearSomeThing("showGuestRow");
    clearSomeThing("showExhibitorRow");
    clearSomeThing("removeEvent");
    clearSomeThing("eventForm");
    addClass("activity", "d-none");
    addClass("panel", "d-none");
    if (document.getElementsByClassName("show").length !== 0) {
        closeShow();
    }

    closeShow();
    getElement("viewAllActivity").classList.add("d-none");
}

function closeShow() {
    if (document.getElementsByClassName("show").length !== 0) {
        document.getElementsByClassName("show")[0].classList.remove("show");
    }
}

function createDivWithClass(style) {
    let div = document.createElement("div");
    div.className = style;

    return div;
}

function containerDivAppendChild(container, element, type, i) {
    let card;
    let cardBody = createDivWithClass("card-body text-center");
    let showPanel = document.createElement("button");

    if (i === 0) {
        card = createDivWithClass("carousel-item active card");
    } else {
        card = createDivWithClass("carousel-item card");
    }


    showPanel.innerText = "Show panel";
    showPanel.dataset.pId = element.id;

    switch (type) {
        case 'auction':
            cardBody.appendChild(createElementP("Description: " + element.description));
            cardBody.appendChild(createElementP("Location: " + element.location));

            let div = createDivWithClass("btn-group accordion accordion-flush d-block mb-1");
            div.appendChild(createBtnShowForm(element.id, "ProductPanel", "Add product"));
            div.appendChild(createBtnShowForm(element.id, "GuestPanel", "Add guest"));
            div.appendChild(createBtnShowForm(element.id, "ExhibitorPanel", "Add exhibitor"));

            let showDiv = createDivWithClass("btn-group d-block mb-1");
            showDiv.appendChild(createBtnShowPanel(element.id, "showProduct", "Show product", showFormProductPanel));
            showDiv.appendChild(createBtnShowPanel(element.id, "showGuest", "Show guest", showFormGuestPanel));
            showDiv.appendChild(createBtnShowPanel(element.id, "showExhibitor", "Show exhibitor", showFormExhibitorPanel));

            cardBody.appendChild(showDiv);
            cardBody.appendChild(div);
            break;
        case 'guest':
            cardBody.appendChild(createElementP("Surname: " + element.surname));
            cardBody.appendChild(createElementP("Name: " + element.name));
            break;
        case "exhibitor":
            cardBody.appendChild(createElementP("Surname: " + element.surname));
            cardBody.appendChild(createElementP("Name: " + element.name));
            break;
        case "product":
            cardBody.appendChild(createElementP("Name: " + element.name));
            cardBody.appendChild(createElementP("Color: " + element.color));
            cardBody.appendChild(createElementP("Dimensions: " + element.dimensions));
            cardBody.appendChild(createElementP("Cost: " + element.cost));
            cardBody.appendChild(createElementP("Scope: " + element.scope));
            cardBody.appendChild(createElementP("Category: " + element.category));
            break;
    }

    container.appendChild(card);
    card.appendChild(cardBody);

    cardBody.appendChild(createBtnRemoveSomething(element.id, type));
}

function createBtnShowForm(id, target, innerT) {
    let showPanel = document.createElement("button");
    showPanel.dataset.pId = id;
    showPanel.className = "btn col border border-dark btn-success fs-4 mb-1";
    showPanel.type = "button";
    showPanel.dataset.bsToggle = "collapse";
    showPanel.dataset.bsTarget = "#" + target;
    showPanel.ariaExpanded = "false";
    showPanel.ariaControls = target;
    showPanel.innerText = innerT;

    return showPanel;
}

function createBtnShowPanel(id, target, innerT, func) {
    let showPanel = document.createElement("button");
    showPanel.dataset.pId = id;
    showPanel.dataset.t = target;
    showPanel.className = "btn col border border-dark btn-primary fs-4 mb-1";
    showPanel.type = "button";
    showPanel.innerText = innerT;
    showPanel.dataset.bsToggle = "collapse";
    showPanel.dataset.bsTarget = "#" + target;
    showPanel.ariaExpanded = "false";
    showPanel.addEventListener("click", func);

    return showPanel;
}

function createBtnRemoveSomething(id, type) {
    let btnRemove = document.createElement("button");
    btnRemove.innerText = "Remove " + type;
    btnRemove.dataset.btnId = id;
    btnRemove.className = "btn btn-danger col border border-dark fs-4";
    switch (type) {
        case "auction":
            btnRemove.addEventListener("click", removeActivity);
            break;

        case "product":
            btnRemove.addEventListener("click", removeProduct);
            break;

        case "guest":
            btnRemove.addEventListener("click", removeGuest);
            break;

        case "exhibitor":
            btnRemove.addEventListener("click", removeExhibitor);
            break;
        default:
            break;
    }

    return btnRemove;
}

function createInputLabel(name, innerT) {
    let label = document.createElement("label");
    label.innerText = innerT;
    label.htmlFor = name;
    label.className = "input-group-text same-width mb-1 fs-4";

    return label;
}

function createInputText(name) {
    let description = document.createElement("input");
    description.type = "text";
    description.id = name;
    description.name = name;
    description.required = true;
    description.className = "form-control mb-1 fs-4";
    return description;
}

function createInputDate(name, date) {
    let d = document.createElement("input");
    d.type = "date";
    d.id = name;
    d.name = name;
    d.required = true;
    d.className = "form-control mb-1 fs-4";

    let dS = date.split("T");
    d.value = dS[0];

    return d;
}

function createElementP(innerT) {
    let p = document.createElement("p");
    p.innerText = innerT;
    p.className = "fs-4";

    return p;
}

function addInputForm(grid, name, innerT, activity, date = 0) {
    let col = document.createElement("div");
    if (activity) {
        col.className = "col-md-6 col-sm-6 text-center input-group";
    } else {
        col.className = "col-md-12 col-sm-6 text-center input-group";
    }

    col.appendChild(createInputLabel(name, innerT));

    if (date === 0) {
        col.appendChild(createInputText(name));
    } else {
        col.appendChild(createInputDate(name, date));
    }
    grid.appendChild(col);
}

function changeBtnToEvent() {
    let createEventButton = document.getElementById("formBtn");
    createEventButton.innerText = "Add event";
    createEventButton.removeEventListener("click", createActivity);
    createEventButton.addEventListener("click", createEvent);
}

function changeBtnToActivity() {
    let createActivityButton = document.getElementById("formBtn");
    createActivityButton.innerText = "Add auction";
    createActivityButton.removeEventListener("click", createEvent);
    createActivityButton.addEventListener("click", createActivity);
}

function addEventForm(dateStart, dateEnd) {
    let form = document.getElementById("eventForm");
    let btnRemove = document.getElementById("removeEvent");
    btnRemove.innerText = "";

    let grid = document.createElement("div");
    grid.className = "row";
    form.appendChild(grid);
    addInputForm(grid, "title", "Title", false);
    addInputForm(grid, "location", "Location", false);
    addInputForm(grid, "dateS", "Date start", false, dateStart);
    addInputForm(grid, "dateE", "Date end", false, dateEnd);


    changeBtnToEvent();
}

function removeEventBtn() {
    let btnRemove = document.getElementById("removeEvent");
    let btn = document.createElement("button");
    btn.id = eventId;
    btn.innerText = "Remove event";
    btn.addEventListener("click", removeEvent);
    btn.className = "btn btn-danger border border-dark mb-4 fs-4";
    btnRemove.appendChild(btn);
}

function addActivityForm(id) {
    eventId = id;
    urlEventWithId = urlEvent + "/" + eventId;
    urlActivity = urlEventWithId + "/activity";
    urlActivityWithId = urlActivity + "/";

    removeEventBtn();

    let form = document.getElementById("eventForm");

    form.innerHTML = "";

    let grid = document.createElement("div");
    grid.className = "row";
    form.appendChild(grid);

    addInputForm(grid, "description", "Description", true);
    addInputForm(grid, "location", "Location", true);

    changeBtnToActivity();
}

function removeClass(id, clas) {
    let btn = getElement(id);
    btn.classList.remove(clas);
}

function addClass(id, clas) {
    let btn = getElement(id);
    btn.classList.add(clas);
}

function showFormProductPanel() {
    setActivityId(this.getAttribute("data-p-id"));
    clearSomeThing("showProductRow");
    getProductList();
}

function showFormGuestPanel() {
    setActivityId(this.getAttribute("data-p-id"));
    clearSomeThing("showGuestRow");
    getGuestList();
}

function showFormExhibitorPanel() {
    setActivityId(this.getAttribute("data-p-id"));
    clearSomeThing("showExhibitorRow");
    getExhibitorList();
}

function clearSomeThing(id) {
    let form = document.getElementById(id);
    form.innerHTML = "";
}

function removeEvent() {

    let url = urlActivityWithId + "list";
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            if (result.length === 0) {
                var requestOptions = {
                    method: 'DELETE',
                    headers: myHeaders,
                    redirect: 'follow'
                };
                fetch(urlEventWithId, requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        refreshPage();
                    })
                    .catch(error => {
                        showError(error);
                    });
            } else {
                showError("There are one or more auction. Remove all of them for be able to remove the event");
            }
        })
        .catch(error =>{
            showError(error);
        });

}

function removeActivity() {
    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    activityID = this.getAttribute("data-btn-id");
    urlActivityWithId = urlActivity + "/" + activityID;

    let url = urlActivityWithId;

    fetch(url, requestOptions)
        .then(response => response.text())
        .then(() => {
            refreshPage();
        })
        .catch(error => showError(error));
}

function removeProduct() {
    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    let productId = this.getAttribute("data-btn-id");
    let url = urlActivityWithId + "/product/" + productId;

    fetch(url, requestOptions)
        .then(response => response.text())
        .then(() => {
            refreshPage();
        })
        .catch(error => showError(error));
}

function removeGuest() {
    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    let guestId = this.getAttribute("data-btn-id");
    let url = urlActivityWithId + "/guest/" + guestId;

    fetch(url, requestOptions)
        .then(response => response.text())
        .then(() => {
            refreshPage();
        })
        .catch(error => showError(error));
}

function removeExhibitor() {
    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    let exhibitorId = this.getAttribute("data-btn-id");
    let url = urlActivityWithId + "/exhibitor/" + exhibitorId;

    fetch(url, requestOptions)
        .then(response => response.text())
        .then(() => {
            refreshPage();
        })
        .catch(error => showError(error));
}

function createEvent() {
    myHeaders.append("Content-Type", "application/json");

    let title = document.getElementById("title");
    let location = document.getElementById("location");
    let dateS = document.getElementById("dateS");
    let dateE = document.getElementById("dateE");

    if (checkIsUndefined(title) && checkIsUndefined(location) && checkIsUndefined(dateS) && checkIsUndefined(dateE)) {
        let raw = JSON.stringify({
            "title": title.value,
            "location": location.value,
            "startsAt": dateS.value,
            "endsAt": dateE.value
        });

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        let id = document.createElement("p");

        let url = urlEvent + "/create";

        fetch(url, requestOptions)
            .then(response => response.text())
            .then(result => {
                id.innerText = result.id;
                addEventOnCalendar(title, dateS, dateE, id);
                disappearForm();
                refreshPage();
            })

            .catch(error => showError(error));
    } else {
        showError(error);
    }
}

function createActivity() {
    myHeaders.append("Content-Type", "application/json");

    let description = document.getElementById("description");
    let location = document.getElementById("location");

    if (checkIsUndefined(description) && checkIsUndefined(location)) {
        var raw = JSON.stringify({
            "description": description.value,
            "location": location.value
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        let url = urlEventWithId + "/activity/create";

        fetch(url, requestOptions)
            .then(response => response.text())
            .then(result => {
                refreshPage();
            })
            .catch(error => showError(error));
    } else {
        showError("wrong data");
    }
}

function createGuest() {
    myHeaders.append("Content-Type", "application/json");

    let name = document.getElementById("guestName");
    let surname = document.getElementById("guestSurname");

    if (checkIsUndefined(name) && checkIsUndefined(surname)) {
        var raw = JSON.stringify({
            "name": name.value,
            "surname": surname.value
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        let url = urlActivityWithId + "/guest/create";

        fetch(url, requestOptions)
            .then(response => response.text())
            .then(() => {
                refreshPage();

            })
            .catch(error => showError(error));
    } else {
        showError("wrong data");
    }

}

function createExhibitor() {
    myHeaders.append("Content-Type", "application/json");

    let name = document.getElementById("exhibitorName");
    let surname = document.getElementById("exhibitorSurname");

    if (checkIsUndefined(name) && checkIsUndefined(surname)) {
        var raw = JSON.stringify({
            "name": name.value,
            "surname": surname.value
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        let url = urlActivityWithId + "/exhibitor/create"

        fetch(url, requestOptions)
            .then(response => response.text())
            .then(() => {
                refreshPage();
            })
            .catch(error => showError(error));
    } else {
        showError("wrong data");
    }

}

function createProduct() {
    myHeaders.append("Content-Type", "application/json");
    let name = document.getElementById("productName");
    let color = document.getElementById("productColor");
    let dimensions = document.getElementById("productDimensions");
    let cost = document.getElementById("productCost");
    let scope = document.getElementById("productScope");
    let category = document.getElementById("productCategory");

    if (checkIsUndefined(name) && checkIsUndefined(color) && checkIsUndefined(dimensions) && checkIsUndefined(cost) && checkIsUndefined(scope) && checkIsUndefined(category)) {

        var raw = JSON.stringify({
            "name": name.value,
            "color": color.value,
            "dimensions": dimensions.value,
            "cost": cost.value,
            "scope": scope.value,
            "category": category.value
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        let url = urlActivityWithId + "/product/create";

        fetch(url, requestOptions)
            .then(response => response.text())
            .then(() => {
                refreshPage();
            })
            .catch(error => showError(error));
    } else {
        showError(error);
    }

}

function checkIsEmpty(result, id) {
    if (result.length === 0) {
        addClass(id, "d-none");
    } else {
        removeClass(id, "d-none");
    }
}

function setActivityId(id) {
    urlActivityWithId = urlActivity + "/" + id;
}

function checkIsUndefined(element) {
    return element.value !== "";
}

function showError(error) {
    alert(error);
}

function refreshPage() {
    location.reload();
}

window.οnlοad = initialization();